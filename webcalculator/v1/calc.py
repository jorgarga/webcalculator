import ply.lex as lex
import ply.yacc as yacc

# Enabled tokens.
tokens = (
    'NUMBER',
    'PLUS',
    'MINUS',
    'MULTIPLY',
    'DIVIDE',
    'LPAREN',
    'RPAREN',
)

# Token definition.
t_PLUS     = r'\+'
t_MINUS    = r'\-'
t_MULTIPLY = r'\*'
t_DIVIDE   = r'\/'
t_LPAREN   = r'\('
t_RPAREN   = r'\)'
t_ignore   = r' '

def t_NUMBER(t):
    r'\d+\.?\d*'
    try:
        t.value = int(t.value)
    except ValueError:
        try:
            t.value = float(t.value)
        except ValueError:
            print("Value is not a number %s", t.value)
            t.value = 0
    return t


def t_newline(t):
  r'\n+'
  t.lexer.lineno += len(t.value)


def t_error(t):
    message = "Expression not formed correctly."
    raise ValueError(message)


lexer = lex.lex()

# Precedence rules
precedence = (
    ('left', 'PLUS', 'MINUS'),
    ('left', 'MULTIPLY', 'DIVIDE'),
    ('right','UMINUS'),
)


# Grammar production rules for the parser.
def p_expression(p):
    '''
    expression : expression PLUS expression
               | expression MINUS expression
               | expression MULTIPLY expression
               | expression DIVIDE expression
    '''
    if   p[2] == '+': p[0] = p[1] + p[3]
    elif p[2] == '-': p[0] = p[1] - p[3]
    elif p[2] == '*': p[0] = p[1] * p[3]
    elif p[2] == '/': p[0] = p[1] / p[3]


def p_expression_uminus(p):
    '''
    expression : MINUS expression %prec UMINUS
    '''
    p[0] = -p[2]


def p_expression_number(p):
    '''
    expression : NUMBER
    '''
    p[0] = p[1] 


def p_expression_group(p):
    '''
    expression : LPAREN expression RPAREN
    '''
    p[0] = p[2]


def p_error(p):
    message = "Syntax error in input."
    raise ValueError(message)



parser = yacc.yacc(debug=False,
                   write_tables=False)


def calculate(operation):
    try:
        value =  parser.parse(operation)
        return value
    except Exception as ex:
        raise ex


import base64

from flask import current_app
from flask_jsonpify import jsonify, request
from webcalculator.v1 import bp
from webcalculator.v1 import calc

@bp.route("/calculus")
def calculus():
    """
     - Receive input from user via query parameter codified in base64.
     - Make the calculation.
     - Return the value of the calculation or error.

     Example: 
         /calculus?query=MiAqICgyMy8oMyozKSktIDIzICogKDIqMyk=

     Returns:
         {'error': 'false', 'result': -132.88888888888889}
    """

    message = {}
    try:
        query_argument = request.args.get("query")
        data_bytes = base64.urlsafe_b64decode(query_argument)
        operation = data_bytes.decode("utf-8")
        result = calc.calculate(operation)

        message = {"error": False,
                   "result": result}

    except Exception as ex:
        message = {"error": True,
                   "message": str(ex)}
        
        current_app.logger.warning("URL: {} - Problem: {}".format(request.url,
                                                        str(ex)))
        
    response = jsonify(message)
    return response

from flask import Blueprint
from flask_cors import CORS

bp = Blueprint('v1', __name__)
CORS(bp)

from webcalculator.v1 import v1

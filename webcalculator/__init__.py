"""
Entry point for the API.
"""
import os
import logging
from logging.handlers import RotatingFileHandler

from flask import Flask

def create_app():
    app = Flask(__name__)


    if "DYNO" in os.environ:
        stream_handler = logging.StreamHandler()
        stream_handler.setLevel(logging.DEBUG)
        app.logger.addHandler(stream_handler)
    else:
        if not os.path.exists("logs"):
            os.mkdir("logs")
        file_handler = RotatingFileHandler("logs/application.log",
                                           maxBytes=10240, backupCount=10)
        file_handler.setFormatter(logging.Formatter(
            '%(asctime)s %(levelname)s: %(message)s '))

        file_handler.setLevel(logging.DEBUG)
        app.logger.addHandler(file_handler)

    app.logger.setLevel(logging.DEBUG)
    app.logger.info("Application started")

    from webcalculator.v1 import bp as v1_bp
    app.register_blueprint(v1_bp)

    return app

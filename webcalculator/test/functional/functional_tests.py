import datetime
import json
import pytest
import base64

@pytest.mark.parametrize("test_input,expected",
                         [("0 + 1", 1),
                          ("0.0 + 1.0", 1.0),
                          ("2 + 3", 5),
                          ("41 + 1", 42),
                          ("9.9 + 3", 12.9),
                          ("9.99 + 0.01", 10.0),
                          ("-0  + 1", 1),
                          ("-1.0 + 0", -1.0),
                          ("-2.6 + -3", -5.6),
                          ("-6 + 4", -2),
                          ("-41 + 1", -40),
                          ("-1000 + 3", -997),
                          ("-10 + 3 - 1 + 10", 2),
                          ("10  - 1", 9),
                          ("0 - 1", -1),
                          ("1000.5 - 1", 999.5),
                          ("10 - 1 - 2 - 3 - 4", 0),
                          ("-1  -  -1", 0),
                          ("-1 - -1", 0),
                          ("-1000.5 - 1", -1001.5),
                          ("-1 - -1 - 1", -1),
                          ("1 *  1", 1),
                          ("2 * 5.3", 10.6),
                          ("16 * 16", 256),
                          ("2 * 2 * 3 * 3", 36),
                          ("1 *  -1", -1),
                          ("2 * -5", -10),
                          ("16 * -16", -256),
                          ("2 * -2 * 3 * -3", 36),
                          ("2 * -2 * 3 * 3", -36),
                          ("2 / 2", 1),
                          ("24 / 2.2", 10.909090909090908),
                          ("42 / 6", 7),
                          ("-2 / 2", -1),
                          ("24 / -2", -12),
                          ("-42 / -6", 7),
                          ("12 - 3 * 4", 0),
                          ("12 + 3 * 4", 24),
                          ("12 - 3 * 4", 0),
                          ("12 - 48 / 4", 0),
                          ("12 - -48 / 4", 24),
                          ("(12 - 3) * 4", 36),
                          ("(12 + 3) * -4", -60),
                          ("12 - (3 * 4)", 0),
                          ("((12) - ((3 * 4)))", 0),
                          ("(12 - 48) / 4.3", -8.372093023255815),
                          ("(12 - -48) / 4", 15),
                          ("-(12 - -48) / 4", -15),
                          ("(-(12 - -48)) / 4", -15),
                          ("(-(12 - -48)) / -(4)", 15),
                          (" 0  +     1", 1),
                          (" 0.0  +   1.0", 1.0),
                          (" -2.6   + - 3", -5.6),
                          (" - 6  +  4", -2),
                          ("  1000.5  - 1", 999.5),
                          ("2 *  - 2 * 3  * -3", 36),
                          ("( 12 +   3  ) *  - 4", -60),
                          ("3.1415 * 3 * 3", 28.2735),
                          ("(1/5) * 3 + (4/10)/2", 4/5),
                          ("(5 - (1 * 2 + 1.5 * 2 ))", 0),
                          ])

def test_correct_input(test_client, test_input, expected):
    input_to_bytes = test_input.encode("utf-8")
    bytes_to_base64 = base64.urlsafe_b64encode(input_to_bytes)
    base64_to_string = bytes_to_base64.decode("utf-8")

    response = test_client.get("/calculus?query=" + base64_to_string)
    expected = {"error": False, "result": expected}
    assert response.status_code == 200
    assert json.loads(response.data) == expected


@pytest.mark.parametrize("test_input,expected",
                         [("(12 - -48) / 0", "division by zero"),
                          ("(12 - -48) / -0", "division by zero"),
                          ("(12 - -48) / (5 - 5)", "division by zero"),
                          ("(12 - -48) / (5 - ( 1 * 2 + 1.5 * 2 ))", "float division by zero"),
                          ("(12 - -48) / 1))", "Syntax error in input."),
                          ("2 * 5. 3", "Syntax error in input."),
                          ("-2 * 5. 3", "Syntax error in input."),
                          ("(12 - -48) / (5 - ()", "Syntax error in input."),
                          (" 0. 0  +   1.0", "Syntax error in input."),
                          (" 0.0  +   1. 0", "Syntax error in input."),
                          (" 0.0  +   1 .0", "Expression not formed correctly."),
                          ("0 + q", "Expression not formed correctly."),
                          ("!wgfe + q", "Expression not formed correctly."),
                          ("¿'?", "Expression not formed correctly."),
                          ("", "Syntax error in input."),
                          ("  ", "Syntax error in input."),
                          ])

def test_problemtatic_input(test_client, test_input, expected):
    input_to_bytes = test_input.encode("utf-8")
    bytes_to_base64 = base64.urlsafe_b64encode(input_to_bytes)
    base64_to_string = bytes_to_base64.decode("utf-8")

    response = test_client.get("/calculus?query=" + base64_to_string)
    expected = {"error": True, "message": expected}
    assert response.status_code == 200
    assert json.loads(response.data) == expected

"""
Unit tests for the calculator
"""

import pytest
from webcalculator.v1 import calc

def test_positive_addition():
    assert calc.calculate("0 + 1") == 1
    assert calc.calculate("0.0 + 1.0") == 1.0
    assert calc.calculate("2 + 3") == 5
    assert calc.calculate("41 + 1") == 42
    assert calc.calculate("9.9 + 3") == 12.9
    assert calc.calculate("9.99 + 0.01") == 10.0


def test_negative_addition():
    assert calc.calculate("-0  + 1") == 1
    assert calc.calculate("-1.0 + 0") == -1.0
    assert calc.calculate("-2.6 + -3") == -5.6
    assert calc.calculate("-6 + 4") == -2
    assert calc.calculate("-41 + 1") == -40
    assert calc.calculate("-1000 + 3") == -997
    assert calc.calculate("-10 + 3 - 1 + 10") == 2


def test_positive_substraction():
    assert calc.calculate("10  - 1") == 9
    assert calc.calculate("0 - 1") == -1
    assert calc.calculate("1000.5 - 1") == 999.5
    assert calc.calculate("10 - 1 - 2 - 3 - 4") == 0


def test_negative_substraction():
    assert calc.calculate("-1  -  -1") == 0
    assert calc.calculate("-1 - -1") == 0
    assert calc.calculate("-1000.5 - 1") == -1001.5
    assert calc.calculate("-1 - -1 - 1") == -1


def test_positive_multiplication():
    assert calc.calculate("1 *  1") == 1
    assert calc.calculate("2 * 5.3") == 10.6
    assert calc.calculate("16 * 16") == 256
    assert calc.calculate("2 * 2 * 3 * 3") == 36


def test_negative_multiplication():
    assert calc.calculate("1 *  -1") == -1
    assert calc.calculate("2 * -5") == -10
    assert calc.calculate("16 * -16") == -256
    assert calc.calculate("2 * -2 * 3 * -3") == 36
    assert calc.calculate("2 * -2 * 3 * 3") == -36


def test_positive_division():
    assert calc.calculate("2 / 2") == 1
    assert calc.calculate("24 / 2.2") == 10.909090909090908
    assert calc.calculate("42 / 6") == 7


def test_negative_division():
    assert calc.calculate("-2 / 2") == -1
    assert calc.calculate("24 / -2") == -12
    assert calc.calculate("-42 / -6") == 7


def test_precedence_rules_without_parenthesis():
    assert calc.calculate("12 - 3 * 4") == 0
    assert calc.calculate("12 + 3 * 4") == 24
    assert calc.calculate("12 - 3 * 4") == 0
    assert calc.calculate("12 - 48 / 4") == 0
    assert calc.calculate("12 - -48 / 4") == 24


def test_precedence_rules_with_parenthesis():
    assert calc.calculate("(12 - 3) * 4") == 36
    assert calc.calculate("(12 + 3) * -4") == -60
    assert calc.calculate("12 - (3 * 4)") == 0
    assert calc.calculate("((12) - ((3 * 4)))") == 0
    assert calc.calculate("(12 - 48) / 4.3") == -8.372093023255815
    assert calc.calculate("(12 - -48) / 4") == 15
    assert calc.calculate("-(12 - -48) / 4") == -15
    assert calc.calculate("(-(12 - -48)) / 4") == -15
    assert calc.calculate("(-(12 - -48)) / -(4)") == 15


def test_spaces_are_ignored():
    assert calc.calculate(" 0  +     1") == 1
    assert calc.calculate(" 0.0  +   1.0") == 1.0
    assert calc.calculate(" -2.6   + - 3") == -5.6
    assert calc.calculate(" - 6  +  4") == -2
    assert calc.calculate("  1000.5  - 1") == 999.5
    assert calc.calculate("2 *  - 2 * 3  * -3") == 36
    assert calc.calculate("( 12 +   3  ) *  - 4") == -60


def test_complicated_forms():
    assert calc.calculate("3.1415 * 3 * 3") == 28.2735
    assert calc.calculate("(1/5) * 3 + (4/10)/2") == 4/5
    assert calc.calculate("(5 - (1 * 2 + 1.5 * 2 ))") == 0
    

def test_illegal_operations():
    with pytest.raises(ZeroDivisionError):
        assert calc.calculate("(12 - -48) / 0") == None

    with pytest.raises(ZeroDivisionError):
        assert calc.calculate("(12 - -48) / -0") == None

    with pytest.raises(ZeroDivisionError):
        assert calc.calculate("(12 - -48) / (5 - 5)") == None

    with pytest.raises(ZeroDivisionError):
        assert calc.calculate("(12 - -48) / (5 - ( 1 * 2 + 1.5 * 2 ))") == None

    with pytest.raises(ValueError):
        assert calc.calculate("(12 - -48) / 1))") == None

    with pytest.raises(ValueError):
        assert calc.calculate("2 * 5. 3") == None

    with pytest.raises(ValueError):
        assert calc.calculate("-2 * 5. 3") == None

    with pytest.raises(ValueError):
        assert calc.calculate("(12 - -48) / (5 - ()") == None

    with pytest.raises(ValueError):
        assert calc.calculate(" 0. 0  +   1.0") == None

    with pytest.raises(ValueError):
        assert calc.calculate(" 0.0  +   1. 0") == None

    with pytest.raises(ValueError):
        assert calc.calculate(" 0.0  +   1 .0") == 1.0

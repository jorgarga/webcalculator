Web calculator
--------
Exercise for Futurice.

## Description
Your task is to setup a simple web service to implement a calculator.
The service offers an endpoint that reads a string input and parses it. It should return either an HTTP error code, or a solution to the calculation in JSON form.

An example calculus query:
* Original query: 2 * (23/(33))- 23 * (23)
* With encoding: MiAqICgyMy8oMyozKSktIDIzICogKDIqMyk=


## API Description
Endpoint:
> GET /calculus?query=[input]

The input can be expected to be UTF-8.

Return:
On success: JSON response of format. Example:
> { error: false, result: number }

On error: Either an HTTP error code or JSON. Example:
> { error: true, message: string }

Supported operations: + - * / ( )


## Technical constraints
There are some constraints that you need to follow. These are followed by some tips and ideas that you can choose to follow if you wish.

Required:
- Use a programming language of your choice.
- The API needs to be testable online from Futurice office.
- Consider adding automated tests where it makes sense.
- When writing your code, imagine the service is meant to be released to production (with a low-to-moderate expected load).
- Heroku or AWS might be a good place to publish your service. Please document your deployment process. 
- The source code should be shared, either on public repository or a repository that Futurice can access. For example, GitHub is a good option.

## Deployment to Heroku
Create a Heroku account if you don't one and log in:
> $ heroku login

Create a new application. The name of the application has to be unique in Heroku:
> $ heroku apps:create jorgarga-webcalculator

The previous command will show something like this on the terminal:
```sh
Creating ⬢ jorgarga-webcalculator... done
https://jorgarga-webcalculator.herokuapp.com/ | https://git.heroku.com/jorgarga-webcalculator.git
```

Now configure the needed environment variable to start the flask application:
> heroku config:set FLASK_APP=webcalculator.py

The code can now be pushed. Type the following:
> git push heroku master

Heroku will start a build and re-deploy the app each time the previous command is run.

On the command line something like this will be shown:
```sh
...
remote: Compressing source files... done.
remote: Building source:
remote:
remote: -----> Python app detected
remote: -----> Installing python-3.6.7
remote: -----> Installing pip
...
...
remote: -----> Discovering process types
remote:        Procfile declares types -> web
remote:
remote: -----> Compressing...
remote:        Done: 42.6M
remote: -----> Launching...
remote:        Released v6
remote:        https://jorgarga-webcalculator.herokuapp.com/ deployed to Heroku
remote:
remote: Verifying deploy... done.
To https://git.heroku.com/jorgarga-webcalculator.git
 * [new branch]      master -> master
```

When the process is done, one can start using the system:
> curl -i -H "Accept: application/json" -H "Content-Type: application/json" -X GET https://jorgarga-webcalculator.herokuapp.com/calculus?query=MiAqICgyMy8oMyozKSktIDIzICogKDIqMyk=

Which will show the following answer:
```sh
HTTP/1.1 200 OK
Connection: keep-alive
Server: gunicorn/19.9.0
Date: Mon, 19 Nov 2018 14:20:17 GMT
Content-Type: application/json
Content-Length: 47
Access-Control-Allow-Origin: *
Via: 1.1 vegur

{"error": false, "result": -132.88888888888889}
```

from setuptools import setup


def readme():
    with open("README.md") as f:
        return f.read()


setup(name="webcalculator",
      version="0.1",
      description="A basic calculator for the web.",
      long_description=readme(),
      classifiers=[
          "Development Status :: 4 - Beta",
          "License :: OSI Approved :: MIT License",
          "Programming Language :: Python :: 3.6",
          "Framework :: Flask",
          "Topic :: Internet :: WWW/HTTP :: WSGI :: Application",
      ],
      url="http://github.com/jorgarga/webcalculator",
      license="MIT",
      packages=["webcalculator"],
      setup_requires=["pytest-runner"],
      tests_require=["pytest"],
      zip_safe=False)
